import json
from typing import Dict, List, Optional, Any
import re
import telegram.ext
from telegram.ext import Updater, CommandHandler, CallbackContext, Dispatcher, PicklePersistence
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
import logging
import os

TOKEN = os.environ['BOT_TOKEN']
persistence: PicklePersistence = PicklePersistence("/app/storage/save_file")


def clear_ride(chat_data: Dict):
    chat_data.pop("ride_message_id", None)
    chat_data["driver"] = ""
    chat_data["passengers"].clear()


def send_or_edit_message_text(bot: telegram.Bot, text, chat_id=None, message_id=None, inline_message_id=None,
                              parse_mode=None, disable_web_page_preview=None, disable_notification=None,
                              reply_to_message_id=None, reply_markup=None, timeout=None, **kwargs):
    try:
        if message_id is None and inline_message_id is None:
            bot.send_message(chat_id=chat_id, text=text, parse_mode=parse_mode,
                             disable_web_page_preview=disable_web_page_preview,
                             disable_notification=disable_notification, reply_to_message_id=reply_to_message_id,
                             reply_markup=reply_markup, timeout=timeout, kwargs=kwargs)
        else:
            bot.edit_message_text(chat_id=chat_id, text=text, message_id=message_id, parse_mode=parse_mode,
                                  disable_web_page_preview=disable_web_page_preview, reply_markup=reply_markup,
                                  timeout=timeout, kwargs=kwargs)
    except telegram.error.TimedOut:
        send_or_edit_message_text(bot, text, chat_id, message_id, inline_message_id, parse_mode,
                                  disable_web_page_preview, disable_notification, reply_to_message_id, reply_markup,
                                  timeout, kwargs=kwargs)


def update_ride_message(update: telegram.Update, context: CallbackContext, closed: bool = False):
    context.chat_data["ride_message_id"] = update.effective_message.message_id
    message = context.chat_data["driver"] + " vai dirigir."
    if context.chat_data["passengers"]:
        message += "\nPassageiros:"
        for p in context.chat_data["passengers"]:
            message += "\n" + p
    if closed:
        message += "\n" + print_status(context)
        send_or_edit_message_text(context.bot, chat_id=update.effective_chat.id,
                                  message_id=update.effective_message.message_id,
                                  text=message)
    else:
        send_or_edit_message_text(context.bot, chat_id=update.effective_chat.id,
                                  message_id=update.effective_message.message_id,
                                  text=message + "\nQuem mais vai?",
                                  reply_markup=InlineKeyboardMarkup([
                                      [InlineKeyboardButton("Eu vou", callback_data="go"),
                                       InlineKeyboardButton("Não vou", callback_data="notgo")],
                                      [InlineKeyboardButton("Fechar corrida", callback_data="close")]
                                  ]))


def update_status(name, value, context: CallbackContext):
    if name in context.chat_data["status"]:
        context.chat_data["status"][name] += value
    else:
        context.chat_data["status"][name] = value
        # menu_callback_switcher["trade_"+name] = menu_callback_trade_with


def print_status(context: CallbackContext) -> str:
    text = "Saldo atual: "
    for k, v in context.chat_data["status"].items():
        text += "\n" + k + ": " + "{:.2f}".format(v) + " reais"
    return text


def menu_callback_ride(update: telegram.Update, context: CallbackContext):
    if "ride_message_id" in context.chat_data:
        try:
            context.bot.delete_message(chat_id=update.effective_chat.id, message_id=context.chat_data["ride_message_id"])
        except:
            pass
        clear_ride(context.chat_data)
    context.chat_data["driver"] = update.effective_user.name
    update_ride_message(update, context)


def menu_callback_current_ride(update: telegram.Update, context: CallbackContext):
    context.bot.delete_message(chat_id=update.effective_chat.id, message_id=context.chat_data["ride_message_id"])
    update_ride_message(update, context)


def menu_callback_go(update: telegram.Update, context: CallbackContext):
    username = update.effective_user.name
    if username not in context.chat_data["passengers"] and username != context.chat_data["driver"]:
        context.chat_data["passengers"].append(username)
        update_ride_message(update, context)


def menu_callback_not_go(update: telegram.Update, context: CallbackContext):
    username = update.effective_user.name
    if username in context.chat_data["passengers"]:
        context.chat_data["passengers"].remove(username)
        update_ride_message(update, context)
    if username == context.chat_data["driver"]:
        clear_ride(context.chat_data)
        menu_callback_cancel(update, context)


def menu_callback_close(update: telegram.Update, context: CallbackContext):
    if update.effective_user.name == context.chat_data["driver"]:
        p = context.chat_data["price"] / (len(context.chat_data["passengers"]) + 1)
        update_status(context.chat_data["driver"], context.chat_data["price"] - p, context)
        for passenger in context.chat_data["passengers"]:
            update_status(passenger, -p, context)
        update_ride_message(update, context, True)
        clear_ride(context.chat_data)


def menu_callback_trade(update: telegram.Update, context: CallbackContext):
    username = update.effective_user.name
    people = ""
    for p in context.chat_data["status"].keys():
        if p != username:
            people += "[{\"text\": \"" + p + "\", \"callback_data\": \"trade_" + p + "\"}],"
    send_or_edit_message_text(context.bot, chat_id=update.effective_chat.id,
                              message_id=update.effective_message.message_id,
                              text=username + " deseja fazer uma troca com...",
                              reply_markup="{\"inline_keyboard\": [" + people +
                                           "[{\"text\":\"Cancelar troca\", \"callback_data\": \"cancel\"}]]}")


def menu_callback_trade_with(update: telegram.Update, context: CallbackContext):
    from_username = update.effective_user.name
    data: str = update.callback_query.data
    to_username = data.split("_", maxsplit=1)[1]
    context.bot.delete_message(chat_id=update.effective_chat.id, message_id=update.effective_message.message_id)
    send_or_edit_message_text(context.bot, chat_id=update.effective_chat.id,
                              text="@" + from_username + ", degite o valor para transferir para @" + to_username)


def menu_callback_status(update: telegram.Update, context: CallbackContext):
    send_or_edit_message_text(context.bot, chat_id=update.effective_chat.id,
                              message_id=update.effective_message.message_id,
                              text=print_status(context))


def menu_callback_cancel(update: telegram.Update, context: CallbackContext):
    send_or_edit_message_text(context.bot, chat_id=update.effective_chat.id,
                              message_id=update.effective_message.message_id,
                              text="Operação cancelada")


def menu_callback_trade_ack(update: telegram.Update, context: CallbackContext):
    m: telegram.Message = update.effective_message
    r = re.match(r"(@.*) enviou R\$(.*) para (@.*)", m.text)
    to_username = r.groups()[2]

    if update.effective_user.name == to_username:
        from_username = r.groups()[0]
        value = float(r.groups()[1])
        update_status(from_username, value, context)
        update_status(to_username, -value, context)
        send_or_edit_message_text(context.bot, chat_id=m.chat_id, message_id=m.message_id,
                                  text=from_username + " enviou R$" + r.groups()[1] + " para " + to_username)


def menu_callback_start_override(update: telegram.Update, context: CallbackContext):
    m: telegram.Message = update.effective_message
    switcher = {
        0: "Bot reinicializado com sucesso",
        1: "JSON de backup mal formatado"
    }
    i = initialize(update, context)
    send_or_edit_message_text(context.bot, chat_id=m.chat_id, message_id=m.message_id,
                              text=switcher.get(i, "Ocorreu um erro"))


menu_callback_switcher = {
    "ride": menu_callback_ride,
    "current_ride": menu_callback_current_ride,
    "trade_ack": menu_callback_trade_ack,
    "status": menu_callback_status,
    "go": menu_callback_go,
    "notgo": menu_callback_not_go,
    "close": menu_callback_close,
    "cancel": menu_callback_cancel,
    "start_override": menu_callback_start_override
}


def menu_callback(update: telegram.Update, context: CallbackContext):
    data = update.callback_query.data
    try:
        menu_callback_switcher.get(data, lambda: None)(update, context)
    except KeyError:
        send_or_edit_message_text(context.bot, chat_id=update.effective_chat.id,
                                  message_id=update.effective_message.message_id,
                                  text="Parece que o bot não foi inicializado corretamente")
    persistence.flush()


def initialize(update: telegram.Update, context: CallbackContext) -> int:
    j = None
    try:
        reply: telegram.Message = update.effective_message.reply_to_message
        if reply is not None:
            j = json.loads(reply.text).items()
    except json.JSONDecodeError:
        return 1
    context.chat_data["status"] = {}
    context.chat_data["driver"] = ""
    context.chat_data["passengers"] = []
    context.chat_data["price"] = 15
    if reply is not None:
        for k, v in j:
            context.chat_data[k] = v
    persistence.flush()
    return 0


def start(update: telegram.Update, context: CallbackContext):
    if len(context.chat_data) > 0:
        if update.effective_message.reply_to_message is None:
            reply_id = None
        else:
            reply_id = update.effective_message.reply_to_message.message_id
        send_or_edit_message_text(context.bot, chat_id=update.effective_chat.id,
                                  text="Parece que o bot já foi inicializado. "
                                       "Deseja sobrescrever o estado atual do bot?",
                                  reply_markup=InlineKeyboardMarkup([[
                                      InlineKeyboardButton("Sobrescrever", callback_data="start_override")]]),
                                  reply_to_message_id=reply_id)
    else:
        initialize(update, context)
        send_or_edit_message_text(context.bot, chat_id=update.effective_chat.id, text="Bot inicializado com sucesso")


def menu(update: telegram.Update, context: CallbackContext):
    try:
        buttons: List[List[InlineKeyboardButton]]
        if context.chat_data["driver"] != "":
            buttons = [[telegram.InlineKeyboardButton("Começar nova carona", callback_data="ride")],
                       [telegram.InlineKeyboardButton("Carona atual", callback_data="current_ride")]]
        else:
            buttons = [[telegram.InlineKeyboardButton("Carona", callback_data="ride")]]
        buttons.append([telegram.InlineKeyboardButton("Status", callback_data="status")])
        send_or_edit_message_text(context.bot, chat_id=update.message.chat_id, text="Escolha o que deseja fazer:",
                                  reply_markup=telegram.InlineKeyboardMarkup(buttons))
    except KeyError:
        send_or_edit_message_text(context.bot, chat_id=update.effective_chat.id,
                                  text="Parece que o bot não foi inicializado corretamente")


def status(update: telegram.Update, context: CallbackContext):
    try:
        send_or_edit_message_text(context.bot, chat_id=update.effective_chat.id, text=print_status(context))
    except KeyError:
        send_or_edit_message_text(context.bot, chat_id=update.effective_chat.id,
                                  text="Parece que o bot não foi inicializado corretamente")


def set_price(update: telegram.Update, context: CallbackContext):
    if len(context.args) == 0:
        send_or_edit_message_text(context.bot, chat_id=update.message.chat_id,
                                  text="Por favor informe o novo valor da corrida no comando com `/setprice xx.xx`")
        return
    try:
        context.chat_data["price"] = float(context.args[0])
        send_or_edit_message_text(context.bot, chat_id=update.message.chat_id,
                                  text="Preço da carona alterado para: R$" + context.args[0])
        persistence.flush()
    except ValueError:
        send_or_edit_message_text(context.bot, chat_id=update.message.chat_id,
                                  text="Manda um float na moral, pfvr (xx.xx)")


def trade(update: telegram.Update, context: CallbackContext):
    try:
        value = float(context.args[1])
        if value > 0:
            from_username = update.effective_user.name
            to_username = context.args[0]
        else:
            to_username = update.effective_user.name
            from_username = context.args[0]
            value = -value
        send_or_edit_message_text(context.bot, chat_id=update.message.chat_id,
                                  text=from_username + " enviou R$" + str(value) + " para " + to_username,
                                  reply_markup=InlineKeyboardMarkup([[
                                      InlineKeyboardButton("Recebido", callback_data="trade_ack")]]))
    except ValueError:
        send_or_edit_message_text(context.bot, chat_id=update.message.chat_id,
                                  text="Manda um float na moral, pfvr (xx.xx)")
    except IndexError:
        send_or_edit_message_text(context.bot, chat_id=update.message.chat_id,
                                  text="Formato esperado: `/trade @username xx.xx`")
    persistence.flush()


def backup(update: telegram.Update, context: CallbackContext):
    send_or_edit_message_text(context.bot, chat_id=update.effective_chat.id, text=context.chat_data)


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
    try:
        updater = Updater(token=TOKEN, use_context=True, persistence=persistence)
        dispatcher: Dispatcher = updater.dispatcher

        start_handler = CommandHandler('start', start)
        dispatcher.add_handler(start_handler)

        menu_handler = CommandHandler('menu', menu)
        dispatcher.add_handler(menu_handler)

        price_handler = CommandHandler('setprice', set_price)
        dispatcher.add_handler(price_handler)

        trade_handler = CommandHandler('trade', trade)
        dispatcher.add_handler(trade_handler)

        status_handler = CommandHandler('status', status)
        dispatcher.add_handler(status_handler)

        backup_handler = CommandHandler('backup', backup)
        dispatcher.add_handler(backup_handler)

        callback_handler = telegram.ext.CallbackQueryHandler(menu_callback)
        dispatcher.add_handler(callback_handler)

        updater.start_polling()
    except telegram.error.InvalidToken:
        print("Token inválido!")
