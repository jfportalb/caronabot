# Bot da Carona

Para iniciar o bot, copie o arquivo `.env.example` para `.env` e insira o token do bot.

Depois, basta executar o comando `docker-compose pull && docker-compose up -d`