FROM python:3.7
WORKDIR /app

#RUN echo "http://dl-8.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
#RUN apk --no-cache --update-cache add gcc libffi-dev openssl-dev gfortran build-base wget freetype-dev libpng-dev openblas-dev

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . .

CMD ["python", "bot.py"]
